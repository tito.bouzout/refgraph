import test from "ava"
import { Graph } from "."

// a
// - b
//   - d
// - c
//   - e

const graph = new Graph<object, string>()
const a = { a: 1 }
const b = { b: 1 }
const c = { c: 1 }
const d = { d: 1 }
const e = { e: 1 }

const w = { subject: a, predicate: "child", object: b }
const x = { subject: a, predicate: "child", object: c }
const y = { subject: b, predicate: "child", object: d }
const z = { subject: d, predicate: "child", object: e }
const allTriples = [w, x, y, z]
for (let triple of allTriples) {
  graph.set(triple)
}

test("get", t => {
  t.true(setEqual(graph.match({ subject: a, object: b }), [w]))
  t.true(setEqual(graph.match({ subject: a }), [w, x]))
  t.true(setEqual(graph.match({ subject: a, object: c }), [x]))
  t.true(
    setEqual(graph.match({ subject: a, predicate: "child", object: b }), [w])
  )
  t.true(setEqual(graph.match({ predicate: "child" }), allTriples))
  t.true(setEqual(graph.match({ object: c }), [x]))
})

test("values", t => {
  t.true(setEqual(graph.values(), allTriples))
  t.true(setEqual(graph[Symbol.iterator](), allTriples))
})

test("traversal", t => {
  t.deepEqual(
    [
      ...graph
        .v(a)
        .as("first")
        .collect()
    ],
    [{ first: a }]
  )
  t.deepEqual([...graph.v(a)], [a])
  t.deepEqual([...graph.v(a).out("child")], [b, c])
  t.deepEqual(
    [
      ...graph
        .v(a)
        .as("first")
        .out("child")
        .as("second")
        .collect()
    ],
    [{ first: a, second: b }, { first: a, second: c }]
  )
  t.deepEqual([...graph.v(a).outE()], ["child", "child"])
  t.deepEqual([...graph.v(b).inE()], ["child"])
  t.deepEqual(
    [
      ...graph
        .v(a)
        .outE()
        .outV()
    ],
    [b, c]
  )
  t.deepEqual(
    [
      ...graph
        .v(a)
        .outE()
        .outV(b)
    ],
    [b]
  )
  t.deepEqual(
    [
      ...graph
        .v(a)
        .out()
        .filter(x => x === b)
    ],
    [b]
  )
  t.deepEqual(
    [
      ...graph
        .v(a)
        .out()
        .filterTriple(t => t.subject === a && t.object === b)
    ],
    [b]
  )
  t.deepEqual([...graph.v(a).out("child", b)], [b])
})

function setEqual<T>(a: Iterable<T>, b: Iterable<T>): boolean {
  const c = new Set(a)
  const d = new Set(b)
  if (c.size !== d.size) {
    return false
  }
  for (let x of c) {
    if (d.has(x)) {
      d.delete(x)
    } else {
      return false
    }
  }
  return true
}
