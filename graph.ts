import { Triple } from "./triple"
import { MultikeyMap, SubArray } from "@seangenabe/multikey-map"
import { VertexTraversal } from "graph-traversal"

/**
 * In-memory reference-based graph database / graph data structure.
 */
export class Graph<V, E, T extends Triple<V, E> = Triple<V, E>>
  implements Iterable<T> {
  private spo = new MultikeyMap<[V, E, V], T>()
  private po = new MultikeyMap<[E, V], T>()
  private so = new MultikeyMap<[V, V], T>()
  private o = new MultikeyMap<[V], T>()

  /**
   * The number of edges in the graph.
   */
  get size() {
    return this.spo.size
  }

  /**
   * Creates a new graph.
   */
  constructor() {}

  /**
   * Inserts a triple into the data structure. If a triple with the same subject, predicate, and object already exists in the graph, it will be replaced with the new object.
   * @param triple The triple to insert.
   */
  set(triple: T): this {
    for (const [indexKey, indexPositions] of indexKeyToIndexPositions) {
      this[indexKey].set(indexPositions.map(pos => triple[pos]) as any, triple)
    }
    return this
  }

  /**
   * Deletes a triple from the graph.
   * @param triple The triple to delete.
   */
  delete(triple: T): boolean {
    let ret = false
    for (const [indexKey, indexPositions] of indexKeyToIndexPositions) {
      ret =
        ret &&
        this[indexKey].delete(indexPositions.map(pos => triple[pos]) as any)
    }
    return ret
  }

  /**
   * Finds matching triples from the graph. The pattern is an object that can have any combination of the keys `subject`, `predicate`, or `object`.
   * @param pattern The pattern to match.
   */
  *match(pattern: Partial<Triple<V, E>>): IterableIterator<T> {
    const positionsInPattern = new Set(getPatternPositions(pattern))
    const indexKey = combinationsToIndex(positionsInPattern)
    const index = this[indexKey]
    const positionsInIndex = indexKeyToIndexPositions.get(indexKey)!
    const partialKey: (V | E)[] = []
    for (const pos of positionsInIndex) {
      if (pos in pattern) {
        partialKey.push(pattern[pos]!)
      } else {
        break
      }
    }
    yield* prefixValues<(V | E)[], T>(index, partialKey)
  }

  /**
   * Iterates through all the triples in the graph.
   */
  values(): IterableIterator<T> {
    return this.match({})
  }

  /**
   * Iterates through all the triples in the graph.
   */
  [Symbol.iterator](): IterableIterator<T> {
    return this.values()
  }

  /**
   * Creates a new traversal from the specified vertex.
   * @param vertex The vertex.
   */
  v(vertex: V): VertexTraversal<V, E> {
    return VertexTraversal.from(this, vertex)
  }
}

/**
 * Gets all the index positions in the given pattern.
 * @param pattern The pattern.
 */
function getPatternPositions(pattern: Partial<Triple<unknown, unknown>>) {
  return (["subject", "predicate", "object"] as IndexPosition[]).filter(
    key => key in pattern
  )
}

function* prefixValues<K extends any[], V>(
  map: MultikeyMap<K, V>,
  prefixKey: SubArray<K>
) {
  for (let [, v] of map.prefixEntries(prefixKey)) {
    yield v
  }
}

type IndexKey = "spo" | "po" | "so" | "o"

function combinationsToIndex(combinations: Set<IndexPosition>): IndexKey {
  if (combinations.size === 1) {
    if (combinations.has("object")) {
      return "o"
    }
  } else if (combinations.size === 2) {
    if (combinations.has("object")) {
      if (combinations.has("predicate")) {
        return "po"
      } else if (combinations.has("subject")) {
        return "so"
      }
    }
  }
  return "spo"
}

const indexKeyToIndexPositions = new Map<IndexKey, IndexPosition[]>([
  ["spo", ["subject", "predicate", "object"]],
  ["so", ["subject", "object"]],
  ["po", ["predicate", "object"]],
  ["o", ["object"]]
])

// = "subject" | "predicate" | "object"
type IndexPosition = keyof Triple<unknown, unknown>
